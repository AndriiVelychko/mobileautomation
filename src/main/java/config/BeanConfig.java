package config;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import packageWithBeans.steps.AppiumRegistrationSteps;
import packageWithBeans.steps.WebRegistrationSteps;
import packageWithBeans.pages.HomePage;
import packageWithBeans.pages.HomePageAppium;
import packageWithBeans.pages.RegistrationPage;
import packageWithBeans.pages.RegistrationPageAppium;

import java.net.MalformedURLException;

@Configuration
@ComponentScan("src.main.java")
@ComponentScan("config")
public class BeanConfig {

    @Bean
    @Profile("APPIUM")
    public AppiumRegistrationSteps appiumRegistrationSteps() {
        return new AppiumRegistrationSteps();
    }


    @Bean
    @Profile("WEB")
    public WebRegistrationSteps webRegistrationSteps() {
        return new WebRegistrationSteps();
    }


    @Bean
    public HomePage homePage(IConfig<WebDriver> config) throws MalformedURLException {
        return new HomePage(config);
    }

    @Bean
    @Profile("WEB")
    public WebConfig webConfig() throws MalformedURLException {
        return new WebConfig();
    }

    @Bean
    @Profile("APPIUM")
    public AppiumConfig appiumConfig() throws MalformedURLException {
        return new AppiumConfig();
    }

    @Bean
    public HomePageAppium homePageAppium(IConfig<AndroidDriver> config) throws MalformedURLException {
        return new HomePageAppium(config);
    }

    @Bean
    public RegistrationPageAppium registrationPageAppium(IConfig<AndroidDriver> config) {
        return new RegistrationPageAppium(config);
    }

    @Bean
    public RegistrationPage registrationPage(IConfig<WebDriver> config) {
        return new RegistrationPage(config);
    }


}
