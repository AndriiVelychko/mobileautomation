package config;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

@Component
@Profile("WEB")
public class WebConfig implements IConfig<WebDriver> {
    private static WebDriver driverInstance;

    public WebConfig() throws MalformedURLException {
        setDriver();
    }

    @Override
    public WebDriver setDriver() throws MalformedURLException {
        if (driverInstance != null) {
            return driverInstance;
        }
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        Map<String, String> mobileEmulation = new HashMap<>();
        mobileEmulation.put("deviceName", "Pixel 2");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
        WebDriverManager.chromedriver().setup();
        driverInstance = new ChromeDriver(chromeOptions);
        return driverInstance;
    }


    @Override
    public WebDriver getDriver() {
        return driverInstance;
    }

    @Override
    public void tearDown() {
        if (driverInstance != null) {
            driverInstance.close();
        }
    }
}
