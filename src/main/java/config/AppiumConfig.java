package config;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

@Component
@Profile("APPIUM")
public class AppiumConfig implements IConfig<AndroidDriver> {
    private static AndroidDriver driverInstance;

    public AppiumConfig() throws MalformedURLException {
        setDriver();
    }

    @Override
    public AndroidDriver setDriver() throws MalformedURLException {
        if (driverInstance != null) {
            return driverInstance;
        }
        File app = new File("src\\main\\resources\\Rozetka.apk");
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setCapability("platformName", Platform.ANDROID);
        capability.setCapability("platformVersion", "11");
        capability.setCapability("udid", "emulator-5554");
        capability.setCapability("app", app.getAbsolutePath());
        capability.setCapability("automationName", "appium");
        driverInstance = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capability);
        return driverInstance;
    }


    @Override
    public AndroidDriver getDriver() {
        return driverInstance;
    }

    @Override
    public void tearDown() {
        if (driverInstance != null) {
            driverInstance.quit();
        }
    }
}
