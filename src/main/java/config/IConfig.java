package config;

import java.net.MalformedURLException;

public interface IConfig<T> {
    T setDriver() throws MalformedURLException;
    T getDriver();
    void tearDown();
}
