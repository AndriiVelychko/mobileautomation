package packageWithBeans.pages;

import config.IConfig;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;


@Component
@Profile("APPIUM")
public class HomePageAppium extends BasePageAppium {

    private AndroidDriver driver;

    @Autowired
    public HomePageAppium(IConfig<AndroidDriver> config) {
        super(config);
    }


    @AndroidFindBy(id = "ua.com.rozetka.shop:id/graph_more")
    private WebElement more;
    @AndroidFindBy(id = "ua.com.rozetka.shop:id/item_menu_auth_tv_sign_up")
    private WebElement registr;

    public WebElement getMore() {
        return more;
    }

    public WebElement getRegistr() {
        return registr;
    }


    public String getsds() {
        return "home page appium";
    }

}
