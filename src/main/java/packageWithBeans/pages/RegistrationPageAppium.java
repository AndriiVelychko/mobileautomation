package packageWithBeans.pages;

import config.IConfig;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("APPIUM")
public class RegistrationPageAppium extends BasePageAppium {

    @Autowired
    public RegistrationPageAppium(IConfig<AndroidDriver> config) {
        super(config);
    }


    @AndroidFindBy(uiAutomator = "new UiSelector().resourceId(\"ua.com.rozetka.shop:id/sign_up_et_first_name\")")
    private WebElement firstName;
    @AndroidFindBy(uiAutomator = "new UiSelector().resourceId(\"ua.com.rozetka.shop:id/sign_up_et_last_name\")")
    private WebElement lastName;
    @AndroidFindBy(uiAutomator = "new UiSelector().resourceId(\"ua.com.rozetka.shop:id/sign_up_et_login\")")
    private WebElement telephoneOrEmail;
    @AndroidFindBy(uiAutomator = "new UiSelector().resourceId(\"ua.com.rozetka.shop:id/sign_up_et_password\")")
    private WebElement password;
    @AndroidFindBy(uiAutomator = "new UiSelector().resourceId(\"ua.com.rozetka.shop:id/sign_up_b_register\")")
    private WebElement submit;
    @AndroidFindBy(uiAutomator = "new UiSelector().resourceId(\"ua.com.rozetka.shop:id/textinput_error\")")
    private WebElement error;

    public WebElement getError() {
        return error;
    }

    public WebElement getFirstName() {
        return firstName;
    }

    public WebElement getLastName() {
        return lastName;
    }

    public WebElement getTelephoneOrEmail() {
        return telephoneOrEmail;
    }

    public WebElement getPassword() {
        return password;
    }

    public WebElement getSubmit() {
        return submit;
    }

}
