package packageWithBeans.pages;

import config.IConfig;
import config.WaitConfig;

import utils.WaitUtils;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class BasePageAppium {

    private AndroidDriver driver;

    public BasePageAppium(IConfig<AndroidDriver> config) {
        WaitConfig waitConfig = new WaitConfig.Builder()
                .setDefaultImplicitTimeUnit(TimeUnit.SECONDS)
                .setDefaultImplicitTimeout(10)
                .setDefaultExplicitTimeout(10)
                .build();
        WaitUtils.setWaitConfig(waitConfig);
        this.driver = config.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    public AndroidDriver getDriver(){
        return driver;
    }
}
